pub struct Post {
    id: u128,
    content: String,
}

impl Post {
    pub fn new(id: u128, content: String) -> Self {
        Post {
            id,
            content,
        }
    }

    pub fn set_content(&self, content: String) -> &Self {
        self.content = content;
        
        self
    }

    pub fn has_id(&self, id: u128) -> bool {
        self.id == id
    }
}
