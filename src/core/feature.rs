pub struct Feature {
    id: u128,
    feature_type: String,
}

impl Feature {
    pub fn new(id: u128, feature_type: String) -> Self {
        Feature { id, feature_type }
    }

    pub fn change_type(&mut self, feature_type: String) -> &mut Self {
        self.feature_type = feature_type;

        self
    }

    pub fn has_id(&self, id: u128) -> bool {
        self.id == id
    }

    pub fn has_type(&self, feature_type: String) -> bool {
        self.feature_type == feature_type
    }
}
