use crate::core::feature::Feature;

pub struct User {
    id: u128,
    username: String,
    password_hash: String,
    features: Vec<Feature>,
    name: Option<String>,
    surname: Option<String>,
}

impl User {
    pub fn new(
        id: u128,
        username: String,
        password_hash: String,
        features: Vec<Feature>,
    ) -> Self {
        User {
            id,
            username,
            password_hash,
            features,
            name: None,
            surname: None,
        }
    }

    pub fn set_password(&mut self, password_hash: String) -> &mut Self {
        self.password_hash = password_hash;

        self
    }

    pub fn set_username(&mut self, username: String) -> &mut Self {
        self.username = username;

        self
    }

    pub fn set_name(&mut self, name: String) -> &mut Self {
        self.name = Some(name);

        self
    }

    pub fn set_surname(&mut self, surname: String) -> &mut Self {
        self.surname = Some(surname);

        self
    }

    pub fn has_id(&self, id: u128) -> bool {
        self.id == id
    }

    pub fn has_username(&self, username: String) -> bool {
        self.username == username
    }

    pub fn has_features(&self, feature_id: u128) -> bool {
        self.features.iter().any(|&x| x.has_id(feature_id))
    }
}
