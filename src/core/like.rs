use crate::core::{post::Post, user::User};

pub struct Like {
    id: u128,
    owner: User,
    post: Post,
}

impl Like {
    pub fn new(id: u128, owner: User, post: Post) -> Self {
        Like {
            id,
            owner,
            post,
        }
    }

    pub fn set_owner(&mut self, owner: User) -> &mut Self {
        self.owner = owner;

        self
    }

    pub fn set_post(&mut self, post: Post) -> &mut Self {
        self.post = post;

        self
    }

    pub fn has_id(&self, id: u128) -> bool {
        self.id == id
    }

    pub fn has_owner(&self, owner_id: u128) -> bool {
        self.owner.has_id(owner_id)
    }

    pub fn is_assigned_to_post(&self, post_id: u128) -> bool {
        self.post.has_id(post_id)
    }
}
