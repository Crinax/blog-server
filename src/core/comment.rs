use crate::core::post::Post;

pub struct Comment {
    id: u128,
    text: String,
    post: Post,
    is_changed: bool,
    comment: Option<Box<Comment>>,
}

impl Comment {
    pub fn new(id: u128, text: String, post: Post) -> Self {
        Comment {
            id,
            text,
            post,
            is_changed: false,
            comment: None,
        }
    }

    pub fn change_text(&mut self, text: String) -> &mut Self {
        self.text = text;
        self.is_changed = true;

        self
    }

    pub fn set_comment(&mut self, comment: Comment) -> &mut Self {
        self.comment = Some(Box::new(comment));

        self
    }

    pub fn set_post(&mut self, post: Post) -> &mut Self {
        self.post = post;

        self
    }

    pub fn has_id(&self, id: u128) -> bool {
        self.id == id
    }

    pub fn is_assigned_to_post(&self, post_id: u128) -> bool {
        self.post.has_id(id)
    }

    pub fn is_assigned_to_comment(&self, comment_id: u128) -> bool {
        match self.comment {
            None => false,
            Some(comment) => comment.has_id(comment_id),
        }
    }
}
